from flask import Flask, request, render_template, session, redirect
import sqlite3
import hashlib
from forex_python.converter import CurrencyRates

app = Flask(__name__)

# Connect to the SQLite database
conn = sqlite3.connect('remittance_db.sqlite')
cursor = conn.cursor()

# Create sender and receiver tables (if not already created)

sender_table = '''
CREATE TABLE IF NOT EXISTS senders (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    first_name TEXT,
    last_name TEXT,
    email TEXT,
    phone TEXT,
    date_of_birth TEXT,
    password TEXT,
    receiver_id INTEGER,
    FOREIGN KEY (receiver_id) REFERENCES receivers(id)
)
'''
cursor.execute(sender_table)

receiver_table = '''
CREATE TABLE IF NOT EXISTS receivers (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    email TEXT,
    phone TEXT,
    address TEXT
)
'''
cursor.execute(receiver_table)

conn.commit()


# Create the route for home
@app.route("/")
def home():
    return render_template("index.html")


# Create the route for sign up
@app.route("/signup", methods=["GET", "POST"])
def signup():
    if request.method == "POST":
        # Get user input from the form
        first_name = request.form.get("first_name")
        last_name = request.form.get("last_name")
        email = request.form.get("email")
        phone = request.form.get("phone")
        date_of_birth = request.form.get("date_of_birth")
        password = request.form.get("password")
        repeat_password = request.form.get("repeat_password")

        # Check if passwords match
        if password != repeat_password:
            return "Passwords do not match"

        # Hash the password
        hashed_password = hashlib.sha256(password.encode()).hexdigest()

        # Insert user data into the senders table
        cursor.execute(
            "INSERT INTO senders (first_name, last_name, email, phone, date_of_birth, password) VALUES (?, ?, ?, ?, ?, ?)",
            (first_name, last_name, email, phone, date_of_birth, hashed_password)
        )

        conn.commit()

        return "Sign up successful"

    # Render the signup.html template
    return render_template("signup.html")
@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        # Get user input from the form
        email = request.form.get("email")
        password = request.form.get("password")

        # Hash the password
        hashed_password = hashlib.sha256(password.encode()).hexdigest()

        # Check if the user exists in the senders table
        cursor.execute("SELECT id, first_name FROM senders WHERE email = ? AND password = ?", (email, hashed_password))
        user = cursor.fetchone()

        if user:
            # Store user information in the session
            session['user_id'] = user[0]
            session['first_name'] = user[1]

            return redirect("/dashboard")

        return "Invalid email or password"

    # Render the login.html template
    return render_template("login.html")


# Create the route for dashboard
@app.route("/dashboard")
def dashboard():
    # Check if the user is logged in
    if 'user_id' in session and 'first_name' in session:
        return f"Welcome, {session['first_name']}! This is your dashboard."
    else:
        return redirect("/login")
def send_money():
    # Check if the user is logged in
    if 'user_id' in session:
        # Get user input from the form
        from_currency = request.form.get("from_country")
        to_currency = request.form.get("to_country")
        amount = 200  # Replace this with the actual amount from the form

        # Perform the logic for sending money
        c = CurrencyRates()
        convert_amount = c.convert(from_currency, to_currency, amount)

        return f"{amount} {from_currency} is equal to {convert_amount} {to_currency}"

    return redirect("/login")  # Redirect the user to login if not logged in

@app.route("/contact")
def contact():
    return render_template("contact.html")




if __name__ == '__main__':
    app.run(debug=True)
