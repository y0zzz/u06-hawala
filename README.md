# u06-hawala 

# Money Transfer Application

The Money Transfer Application is a simple web application that allows users to send money from Sweden to Somalia. Users can sign up, log in, and initiate money transfers to recipients in Somalia. The application also provides a list of previous money transfers for each user.

# Features

* User signup: Users can create an account by providing their name, email, and password.

* User login: Registered users can log in to their account using their email and password. 

* Money transfer: Logged-in users can initiate money transfers by providing the recipient's information and the transfer amount.

* Transaction history: Each user has access to a list of their previous money transfers, including details such as recipient information and transfer amounts.

# Requirements

To run the Money Transfer Application, you need the following requirements:

* Python 3.x

* Flask framework

* forex_python library (for currency conversion)

* SQLAlchemy (for database operations)
 
 # Running the Application

 python app.py



