import unittest
from forex import convert_currency

class CurrencyConversionTestCase(unittest.TestCase):
    def test_currency_conversion(self):
        amount = 200
        from_currency = 'USD'
        to_currency = 'SEK'
        convert_amount = convert_currency(amount, from_currency, to_currency)

        expected_output = f'{amount} {from_currency} is equal to {convert_amount} {to_currency}'
        self.assertAlmostEqual(convert_amount, 1725.4, places=1)
        self.assertEqual(str(convert_amount), expected_output)

if __name__ == '__main__':
    unittest.main()
