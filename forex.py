from forex_python.converter import CurrencyRates

def convert_currency(amount, from_currency, to_currency):
    c = CurrencyRates()
    convert_amount = c.convert(from_currency, to_currency, amount)
    return convert_amount

amount = 200
from_currency = 'USD'
to_currency = 'SEK'
convert_amount = convert_currency(amount, from_currency, to_currency)

print(f'{amount} {from_currency} is equal to {convert_amount} {to_currency}')
